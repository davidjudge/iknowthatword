# API ROUTES

## 1. Requesting translations

### 1.1. Requesting translations and definitions WITHOUT LOGIN

GET `/api/translate/{word_in_english}`

```javascript
{
    translations: [
        { translation: '...' },
        { translation: '...' },
        { translation: '...' },
        ...
    ],
    definition: '...'
}
```

### 1.2. Requesting translations and definitions WITH LOGIN

POST `/api/translate/{word_in_english}`

This will not just returns the translation, but also stores that

```javascript
{
     translations: [
        { translation: '...' },
        { translation: '...' },
        { translation: '...' },
        ...
    ],
    definition: '...'
}
```

## 2. Handling pastes

### 2.1. Saving the text

POST `/api/paste/save`

You have to send the text and the token:

```javascript
{
    text: '...',
}
```
### 2.2. Deleting the text

POST `/api/paste/delete/{id}`
You have to send the token

### 2.3. Showin a text

POST `/api/paste/show/{id}`

You have to send the token
This will give you a response:

```javascript
{
    text: '...'
}
```

### 2.4. Listing the user's pastes
POST `/api/paste/list`

You have to send the token

This will give you the pasted texts by the user:

```javascript
{
    texts: [
        {
            id: '...',
            text: '...',
            created_at: '...'
        }
    ]
}
```

## Requesting statistics for a user
POST `/api/userstat`

You have to send the token
(coverage means that how many words did he know from his pastes)
You will get the response:
```javascript
{
    coverage: 1,
    known_word_count: 123,
    unknown_word_count: 123,
    unknown_words: [
        { word: "..." },
        { word: "..." },
        { word: "..." }
    ] 
}
```

## Requesting statistics for a word
GET `/api/wordstat/{word}`

You will get the response:
```javascript
{
    known_count: 123,
    unknown_count: 123,
    learned_count: 123
}
```

## Requesting global statistics
GET `/api/globalstat`

You will get the response:
```javascript
{
    hardest_words: [
        { 
            word: '...', 
            known_count: 123, 
            unknown_count: 123, 
            learned_count: 123 
        },
        ...
    ]
}
```

## Asking words

POST `/api/ask`

You have to send a request with the asked word and with the answer

```javascrpit
{
    asked: '...',
    answered: '...'
}
```

You will get a boolean feedback:

```javascrpit
{
    result: true,
    corrects: [
        {translation: '...'},
        {translation: '...'},
        {translation: '...'}
    ],
}
```