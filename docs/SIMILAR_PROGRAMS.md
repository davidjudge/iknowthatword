# Hasonló programok

## vocabulary.com
https://www.vocabulary.com/
Egynyelvű, nem túl interaktív.
Nincs szöveg környezet
## vocab1
Egynyelvű, van kiejtés.
Értesítéseket küld benne a szóval és a defincítóval.
Nem lehet bemásolni saját szöveget.
Nincs 
A legnagyobb hátránya, hogy nem webes felületű, így nem érhető el mobilon sem.

##Vocabulary Super Stretch
Nem webes felületű.
Nagyon régi, rossz kinézettel.
Van szövegkörnyezet a szavakhoz.
Lehet párosítani a szavakat rokonértelműekkel.
Nem lehet bemásolni saját szöveget.
