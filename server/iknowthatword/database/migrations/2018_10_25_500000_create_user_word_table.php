<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_word', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->index();
            $table->string('word')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('word')->references('word')->on('words');
            $table->integer('unknown_count')->default(0);
            $table->integer('known_count')->default(0);
            $table->integer('asked_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_word');
    }
}
