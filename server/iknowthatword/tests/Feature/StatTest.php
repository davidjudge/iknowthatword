<?php

namespace Tests\Feature;
use Tests\Concerns\AttachJwtToken;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
class StatTest extends TestCase
{
    use AttachJwtToken;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create();

        $response = $this->loginAs($user)->json('POST', '/api/userstat', []);
        $this->assertTrue($response->decodeResponseJson()['coverage'] == 0);

        $user->pastes()->createMany([
            ["text" => "hello my name is David"],
            ["text" => "and php is awesome"]
        ]);

        $response = $this->loginAs($user)->json('POST', '/api/userstat', []);
        $this->assertTrue($response->decodeResponseJson()['coverage'] == 1);
    }
}
