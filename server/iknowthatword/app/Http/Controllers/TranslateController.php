<?php

namespace App\Http\Controllers;

use App\Translate;
use App\User;
use App\Word;
use Illuminate\Support\Facades\Auth;
use App\UserWord;
use Illuminate\Http\Request;
use App\Repository\WordRepository;
class TranslateController extends Controller
{

    public function translate(Request $request, $word)
    {
        return $this->sendTranslation($this->getTranslation($word));
    }

    public function translateForUser(Request $request, $word)
    {
        $translations = $this->getTranslation($word);

        if(empty($translations['translations']))
            return $this->sendTranslation($translations);
        WordRepository::addUnknownWord($word);
        return $this->sendTranslation($translations);
    }

    private function getTranslation($word) {
        return [
            "translations" => array_map(function ($val) {
                return ["translation" => $val];
            }, Translate::translate($word)),
            "definition" => '...',
        ];
    }

    private function sendTranslation($array) {
        return response()->json($array, 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }
}
