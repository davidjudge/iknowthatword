<?php

namespace App\Http\Controllers;

use App\Word;
use App\Translate;
use App\UserWord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repository\WordRepository;

class StatController extends Controller
{
    public function wordStat(Request $request, $word)
    {
        return Word::where('word', $word)->first();
    }

    public function globalStat()
    {
        return [
            "hardest_words" => Word::orderBy('unknown_count', 'desc')->take(10)->get(),
        ];
    }

    public function userStat()
    {
        $user = Auth::user();

        $words = $user->pastes->reduce(function ($carry, $item){
                $t = preg_replace("[^0-9a-zA-Z \-]", "", $item->text);
                $t = count(explode(" ", $t));
                return $t + $carry;
            });


        if ($words)
            $coverage = ($words - $user->unknown_count) / $words;
        else
            $coverage = 0;

        return [
            "coverage" => $coverage,
            "known_word_count" => $user->known_count,
            "unknown_word_count" => $user->unknown_count,
            "unknown_words" => $user
                ->words()
                ->where('user_word.known_count', 0)
                ->get(),
        ];
    }

    public function asked(Request $request)
    {
        /*
            {
                asked: '...',
                answered: '...'
            }
        */

        $asked = $request->input("asked");
        $answered = $request->input("answered");

        $translations = Translate::translate($asked);

        if (count($translations)) {
            WordRepository::addAskedWord($asked);
        }

        foreach ($translations as $translate) {
            if ($answered == $translate) {
                WordRepository::addKnownWord($asked);
                return ["result" => true];
            }
        }


        WordRepository::addUnknownWord($asked);

        return [
            "result" => false,
            "corrects" => array_map(function ($val) {
                return ["translation" => $val];
            }, $translations)
        ];
    }
}
