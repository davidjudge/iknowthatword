<?php

namespace App\Repository;

use Illuminate\Support\Facades\Auth;
use App\Word;
use App\UserWord;
class WordRepository {

    public static function addUnknownWord($word) {
        self::addWord($word,"addUnknown");
    }

    public static function addKnownWord($word) {
        self::addWord($word,"addKnown");
    }

    public static function addAskedWord($word) {
        self::addWord($word,"addAsked");
    }

    private static function addWord($word,$type) {
         //increase the unknown count
         $user = Auth::user();
         $user->$type();

         //making a new word if it is not existed yet
         $wordObj = Word::firstOrNew(['word' => $word]);
         $wordObj->$type();

         //making a new word connection if it is not existed yet
         $userWord = UserWord::firstOrNew([
             "user_id" => $user->id,
             "word_id" => $wordObj->id
         ]);
         $userWord->$type();
    }
}
