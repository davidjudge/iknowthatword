<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Countable;

class UserWord extends Model
{
    use Countable;
    protected $table = "user_word";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'word_id'];
    public $timestamps = false;
}
