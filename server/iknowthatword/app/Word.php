<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Countable;
class Word extends Model
{

    use Countable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'word',
    ];

    public $timestamps = false;
}
