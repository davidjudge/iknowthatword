<?php

namespace App\Traits;

trait Countable {

    public function addUnknown() {
        $this->unknown_count++;
        $this->save();
    }

    public function addKnown() {
        $this->known_count++;
        $this->save();
    }

    public function addAsked() {
        $this->asked_count++;
        $this->save();
    }
}
