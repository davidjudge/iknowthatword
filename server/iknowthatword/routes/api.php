<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
return $request->user();
});
 */

 /**
    GET `/api/translate/{word_in_english}`
    POST `/api/translate/{word_in_english}`

    POST `/api/paste/save`
    POST `/api/paste/delete/{id}`
    POST `/api/paste/show/{id}`
    POST `/api/paste/list`

    POST `/api/userstat`
    GET `/api/wordstat/{word}`
    GET `/api/globalstat`

    POST `/api/ask`
  */

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

Route::get('/wordstat/{word}', 'StatController@wordStat');
Route::get('/globalstat', 'StatController@globalStat');

Route::group(['middleware' => ['jwt.verify']], function () {
    // these routes requires login
    Route::get('user', 'UserController@getAuthenticatedUser');

    Route::post('/translate/{word}', 'TranslateController@translateForUser');

    Route::post('/paste/show/{id}', 'PasteController@show');
    Route::post('/paste/list', 'PasteController@index');
    Route::post('/paste/save', 'PasteController@store');
    Route::post('/paste/delete/{id}', 'PasteController@destroy');

    Route::post('/userstat', 'StatController@userStat');

    Route::post('/ask', 'StatController@asked');
});

Route::get('/translate/{word}', 'TranslateController@translate');


