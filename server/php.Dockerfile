FROM php:7-fpm-alpine

RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo_mysql

#RUN apk --no-cache add libmcrypt-dev libmcrypt
#RUN apk --no-cache add mysql-client imagemagick-dev
#
#RUN pecl install imagick \
#    && docker-php-ext-enable imagick \
#    && docker-php-ext-install mcrypt pdo_mysql
