import { Component, OnInit } from '@angular/core';
import {LoginModel} from '../models/login-model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService} from "../services/alert.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: LoginModel = new LoginModel();
  loginForm: FormGroup;
  returnUrl: string;

  /**
   * Password mező láthatóságának állításához szükséges
   */
  hide = true;


  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService, private route: ActivatedRoute, private alertService: AlertService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'email': [this.user.email, [
      Validators.required,
      Validators.email]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6)
      ]]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.authService.login(this.user).subscribe(result => {
      if (result) {
        this.router.navigate([this.returnUrl]);
      } else {
        this.alertService.error('Invalid username and/or password!');
      }
    }, error => {
      this.alertService.error('Invalid username and/or password!');
      console.log(error);
    });
  }

}
