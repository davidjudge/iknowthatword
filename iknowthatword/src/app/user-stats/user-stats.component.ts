import { Component, OnInit } from '@angular/core';
import {StatisticsService} from "../services/statistics.service";

@Component({
  selector: 'app-user-stats',
  templateUrl: './user-stats.component.html',
  styleUrls: ['./user-stats.component.scss']
})
export class UserStatsComponent implements OnInit {

  constructor(private  statService: StatisticsService) { }

  public knownWordCount;
  public knownWordPercentage;

  ngOnInit() {
    this.statService.getUserStats().subscribe(response => {
      this.knownWordCount = response.known_word_count;
      this.knownWordPercentage = response.coverage;
      console.log(response.coverage);
    });
  }

}
