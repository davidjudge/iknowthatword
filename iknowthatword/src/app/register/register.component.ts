import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {RegisterModel} from '../models/register-model';
import {AlertService} from '../services/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  private returnUrl;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private alertService: AlertService,
              private route: ActivatedRoute) {

  }

  user: RegisterModel = new RegisterModel();
  RegisterForm: FormGroup;

  /**
   * Password mező láthatóságának állításához szükséges
   */
  hide = true;

  ngOnInit() {
    this.RegisterForm = this.formBuilder.group({
      'name': [this.user.name, [
        Validators.required]],
      'email': [this.user.email, [
        Validators.required,
        Validators.email]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6)
      ]],
      'password_confirmation': [this.user.password_confirmation, [
        Validators.required,
        Validators.minLength(6)
      ]]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  register() {
    this.authService.register(this.user).subscribe(result => {
      if (result) {
        this.router.navigate([this.returnUrl]);
      } else {
        this.alertService.error('Invalid username and/or password!');
      }
    });
  }

}
