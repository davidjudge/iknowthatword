import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthService {


  currentUser: BehaviorSubject<any>;

  apiUrl = 'http://95.140.38.140:8080';

  constructor(private http: HttpClient) {
    const jwt = new JwtHelperService();
    let token = localStorage.getItem('token');
    if(!jwt.isTokenExpired(token)){
      this.currentUser = new BehaviorSubject(jwt.decodeToken(token));
    }
    else {
      this.currentUser = new BehaviorSubject(null);
    }
  }

  login(credentials) {
    return this.http.post<any>(this.apiUrl + '/api/login',
      credentials).pipe(map(response => {
        const result = response;
        if (result && result.token) {
          localStorage.setItem('token', result.token);
          const jwt = new JwtHelperService();
          this.currentUser.next(jwt.decodeToken(result.token));
          return true;
        }
        return false;
    }));
  }
  register(credentials) {
    return this.http.post<any>(this.apiUrl + '/api/register', credentials).pipe(map(data => {
      const result = data;
      if (result && result.token) {
        localStorage.setItem('token', result.token);
        const jwt = new JwtHelperService();
        this.currentUser.next(jwt.decodeToken(result.token));
        return true;
      }
      return false;
    }));
  }

  logout() {
    localStorage.removeItem('token');
    this.currentUser.next(null);
  }
}
