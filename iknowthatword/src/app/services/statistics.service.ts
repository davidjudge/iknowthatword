import {Injectable, OnDestroy} from '@angular/core';
import {Subscription, BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService{

  currentUserSubs: Subscription;
  currentUser;

  apiUrl = 'http://95.140.38.140:8080';

  constructor(private http:HttpClient) {
    
  }

  getHardestWords(){
    return this.http.get<any>(this.apiUrl+'/api/globalstat',{});
  }

  getSomethingFromUsers(){    
    return this.http.post<any>(this.apiUrl+'/api/userstat',{});
  }

  getUserStats(){
    return this.http.post<any>(this.apiUrl+'/api/userstat',{});
  }
  
}
