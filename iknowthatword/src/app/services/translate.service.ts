import {Injectable, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {AuthService} from "./auth.service";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TranslateService implements OnDestroy{

  currentUserSubs: Subscription;
  currentUser;

  apiUrl = 'http://95.140.38.140:8080';

  constructor(private authService: AuthService, private http:HttpClient) {
    this.currentUserSubs = this.authService.currentUser.subscribe(user => {
      this.currentUser = user
    });
  }

  getTranslation(word: string){
    if(this.currentUser){
      return this.http.post<any>(`${this.apiUrl}/api/translate/${word}`,{});
    }
    else {
      return this.http.get<any>(`${this.apiUrl}/api/translate/${word}`);
    }
  }

  ngOnDestroy(): void {
    this.currentUserSubs.unsubscribe();
  }
}
