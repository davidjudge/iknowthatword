import { Injectable } from '@angular/core';
import {MatSnackBar} from "@angular/material";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public snackBar: MatSnackBar) {

  }

  success(message: string) {
    this.snackBar.open(message, 'Bezár' , {
      duration: 10000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: ['my-success-snack-bar', 'my-success-snack-bar button']
    });
  }

  warning(message: string) {
    this.snackBar.open(message, 'Bezár' , {
      duration: 10000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: ['my-warning-snack-bar']
    });
  }

  error(message: string) {
    this.snackBar.open(message, 'Bezár' , {
      duration: 10000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: ['my-error-snack-bar']
    });
  }

}
