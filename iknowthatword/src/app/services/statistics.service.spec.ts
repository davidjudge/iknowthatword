import { TestBed } from '@angular/core/testing';

import { ExerciseService } from './exercise.service';
import { StatisticsService } from './statistics.service';

describe('AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StatisticsService = TestBed.get(ExerciseService);
    expect(service).toBeTruthy();
  });
});
