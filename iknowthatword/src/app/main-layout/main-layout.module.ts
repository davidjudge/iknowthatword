import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UserBoxComponent } from './user-box/user-box.component';
import { RouterModule } from '@angular/router';
import {AngularMaterialModule} from '../angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AngularMaterialModule
  ],
  declarations: [
    MainLayoutComponent,
    NavBarComponent,
    UserBoxComponent
  ],
  exports: [
    MainLayoutComponent
  ]
})
export class MainLayoutModule { }
