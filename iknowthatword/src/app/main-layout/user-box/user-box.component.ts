import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-user-box',
  templateUrl: './user-box.component.html',
  styleUrls: ['./user-box.component.scss']
})
export class UserBoxComponent implements OnInit {

  currentUserSubs: Subscription;
  currentUser;

  constructor(private authService: AuthService) {
    this.currentUserSubs = this.authService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  logOut() {
    this.authService.logout();
  }

  ngOnInit() {
  }

}
