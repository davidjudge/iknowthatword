import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ExerciseComponent } from './exercise/exercise.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuard} from "./guards/auth.guard";
import { StatisticsComponent } from './statistics/statistics.component';
import { UserStatsComponent } from './user-stats/user-stats.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent},
    { path: 'exercise', component: ExerciseComponent, canActivate: [AuthGuard]},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'statistics', component: StatisticsComponent},
    { path: 'user-stats', component: UserStatsComponent},
    { path: 'login', component: LoginComponent},
    { path: 'register', component: RegisterComponent}
];


@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
