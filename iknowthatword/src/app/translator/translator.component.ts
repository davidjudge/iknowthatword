import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from "../services/translate.service";
import { AlertService } from "../services/alert.service";

@Component({
  selector: 'app-translator',
  templateUrl: './translator.component.html',
  styleUrls: ['./translator.component.scss']
})

export class TranslatorComponent {
  control: FormControl;
  words = [];
  text = new FormControl();
  haveTranslation: boolean;
  translations;

  constructor(private translate: TranslateService, private alert: AlertService) {

  }

  onPaste(e: any): void {
    let clipboardData, pastedData;

    // Stop data actually being pasted into div
    e.stopPropagation();
    e.preventDefault();

    // Get pasted data via clipboard API
    const w: any = window;
    clipboardData = e.clipboardData || w.clipboardData;

    pastedData = clipboardData.getData('text');
    this.words = pastedData.split('\n').map(v => v.split(' ') );
  }

  translateWord(e: Event) {
    (e.target as Element).classList.add('visited');
    this.translate.getTranslation(
      (e.target as Element).innerHTML.toString().replace(/[,;:.?!()`‘'¨”˝"´\s]/g, ''))
      .subscribe(translation => {
        if (translation.translations.length) {
          this.haveTranslation = true;
          this.translations = translation.translations;
          console.log(translation);
        }
        else {
          this.haveTranslation = false;
          this.alert.warning('Ennek a szónak nem található fordítása!');
        }

      });
  }
}
