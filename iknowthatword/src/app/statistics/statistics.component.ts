import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js'
import { StatisticsService } from '../services/statistics.service';
import { HttpClient } from "@angular/common/http";

@Component({
    selector: 'app-dashboard',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

    top_three_hardest_words = [];
    hardest_words = [];

    length = 0;

    constructor(private statistics: StatisticsService, private http: HttpClient) { }

    async ngOnInit() {
        //init phase        
        this.length = 0;

        //get data from api
        await this.statistics.getHardestWords()
            .toPromise()
            .then(response => {
                this.length = response.hardest_words.length;
                
                this.hardest_words = response.hardest_words;
                console.log(response);                
            })
            .catch(err => console.log(err));
        this.hardest_words = this.sortHardestWords();
        this.top_three_hardest_words = this.hardest_words.slice(0,3);
        this.hardest_words = this.hardest_words.slice(3,10);
    }
    sortHardestWords(){
        this.hardest_words.sort(function(a,b){
            return b.unknown_count - a.unknown_count
        })
        return this.hardest_words;        
    }
}
