import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MainLayoutModule } from './main-layout/main-layout.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {AngularMaterialModule} from './angular-material.module';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AuthService} from './services/auth.service';
import { RegisterComponent } from './register/register.component';
import { TranslatorComponent } from './translator/translator.component';
import {AlertService} from './services/alert.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TokenInterceptor} from './helpers/token-interceptor';
import { ExerciseComponent } from './exercise/exercise.component';
import {AuthGuard} from "./guards/auth.guard";
import { StatisticsComponent } from './statistics/statistics.component';
import { UserStatsComponent } from './user-stats/user-stats.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    TranslatorComponent,
    ExerciseComponent,
    StatisticsComponent,
    UserStatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MainLayoutModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    AlertService,
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
