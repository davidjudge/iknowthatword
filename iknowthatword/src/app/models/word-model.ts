export class WordModel {
    word: string;
    translation: string;
    answer: string;
    correct: boolean;
    constructor (word: string){
        this.word=word;
        this.translation = "";
        this.answer = "";
        this.correct=false;
    }
    
}