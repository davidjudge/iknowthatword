import { WordModel } from '../models/word-model'

export const WORDS: WordModel[] = [
    {word: "Love", translation: 'szeretet szerelem szeret szerelmes', answer: "", correct: false},
    {word: "Weak", translation: 'gyenge gyönge beteges', answer: "", correct: false},
    {word: "Strong", translation: 'erős izmos', answer: "", correct: false},
    {word: "Weakling", translation: 'nyápic', answer: "", correct: false},
    {word: "Much", translation: 'sok sokkal nagyon', answer: "", correct: false},
    {word: "Human", translation: 'ember emberi', answer: "", correct: false},

];