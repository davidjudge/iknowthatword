import { Component, OnInit } from '@angular/core';
import { WordModel } from '../models/word-model';
import { WORDS } from './word-templates';
import { HttpClient } from "@angular/common/http";
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExerciseService } from '../services/exercise.service';

@Component({
    selector: 'app-exercise',
    templateUrl: './exercise.component.html',
    styleUrls: ['./exercise.component.scss']
})

export class ExerciseComponent implements OnInit {

    //IMPORT
    apiUrl = 'http://95.140.38.140:8080';
    token;
    temp;
    //currentlySelected
    selectedWord: WordModel;
    length;
    //Mennyin állunk
    barProgress;
    //Ellenörzéskor
    checkingResults;
    //
    noMore;
    //knownWordsCount;
    //unknownWordsCount;
    unknownWordsArray: Array<WordModel> = [];
    correctAnswers;
    constructor(private exercise: ExerciseService, private http: HttpClient) {

    }

    //Init
    ngOnInit() {
        this.barProgress = 0;
        this.checkingResults = false;
        this.correctAnswers = 0;
        this.exercise.getUnknownWordsFromUser().subscribe(response => {
            response.unknown_words.forEach(element => {
                let tempWordModel = new WordModel(element.word);
                this.unknownWordsArray.push(tempWordModel);
            });
            this.selectedWord = this.unknownWordsArray[0];
            this.length = this.unknownWordsArray.length;
        });
    }

    //Mindig meghívjuk ha átmegyünk egy másik kérdésre
    checkFill() {
        let answered = 0;
        this.unknownWordsArray.forEach(function (word) {
            if (word.answer)
                answered++;
        })
        this.barProgress = answered / this.length * 100;

    }
    //Következő gomb megnyomáskor
    onNext(currentlySelected) {
        var index = this.unknownWordsArray.findIndex(word => word == currentlySelected);
        currentlySelected = this.replaceSensitiveData(currentlySelected)
        var data = { asked: currentlySelected.word, answered: currentlySelected.answer };
        this.exercise.postDataForAsk(data).subscribe(response => {
            console.log(response);
            currentlySelected.correct = response.result;
            if (!response.result) {
                currentlySelected.translation = "";
                response.corrects.forEach(translation => {
                    currentlySelected.translation += translation.translation + ', ';
                });
            } else
                this.correctAnswers++;
        });
        if (index < this.length - 1)
            this.selectedWord = this.unknownWordsArray[index + 1];
        else
            this.checkResults();
    }
    //Ha másik kérdést választunk akkor az legyen a selected
    onSelect(word: WordModel) {
        this.selectedWord = word;
    }
    //replaceljük az adatokat amit kell
    replaceSensitiveData(unknownWord) {
        unknownWord.translation = unknownWord.translation.toLowerCase();
        unknownWord.word = unknownWord.word.toLowerCase();
        unknownWord.word = unknownWord.word.replace(/[^A-Za-z\-]/g, '');
        unknownWord.answer = unknownWord.answer.toLowerCase();
        unknownWord.answer = unknownWord.answer.replace(/(\n)/gm, '');
        unknownWord.answer = unknownWord.answer.replace(/[^A-Za-záÁéÉíÍoOóÓöÖőŐÚüűŰ \-]/g, '');
        return unknownWord;
    }



    checkResults() {
        this.checkFill();
        this.correctAnswers = 0;
        let exerciseCompo = this.exercise;

        this.unknownWordsArray.forEach(unknownWord => {
            if (unknownWord.correct) {
                this.correctAnswers++;
                return;
            }

            unknownWord.translation = unknownWord.translation.toLowerCase();
            unknownWord.word = unknownWord.word.toLowerCase();
            unknownWord.word = unknownWord.word.replace(/[^A-Za-z\-]/g, '');
            unknownWord.answer = unknownWord.answer.toLowerCase();
            unknownWord.answer = unknownWord.answer.replace(/(\n)/gm, '');
            unknownWord.answer = unknownWord.answer.replace(/[^A-Za-záÁéÉíÍoOóÓöÖőŐÚüűŰ \-]/g, '');

            var data = { asked: unknownWord.word, answered: unknownWord.answer };
            exerciseCompo.postDataForAsk(data).subscribe(response => {
                unknownWord.correct = response.result;
                if (!response.result) {
                    unknownWord.translation = "";
                    response.corrects.forEach(translation => {
                        unknownWord.translation += translation.translation + ', ';
                    });
                } else
                    this.correctAnswers++;
            })
        })
        this.selectedWord = this.unknownWordsArray[0];
        if (this.correctAnswers == this.length)
            this.noMore = true;
        this.checkingResults = true;
    }

    //Töröljük azokat amiket eltaláltunk
    restart() {
        var tempWords = [];
        this.unknownWordsArray.forEach(word => {
            if (!word.correct)
                tempWords.push(word);
        })
        this.unknownWordsArray = tempWords;
        this.length = this.unknownWordsArray.length;
        this.checkingResults = false;

        this.selectedWord = this.unknownWordsArray[0];
    }
    return() {
        this.noMore = false;
        this.checkingResults = false;
    }

}