# funkcionális követelmények:
Lehessen angol szöveget beilleszteni.
Ha a felhasználó nem akar beilleszteni, választhasson példaszöveget.
A szöveg szavaiból lehessen kattintással fordítani magyarra.
A program mutassa ezeket statisztikákat a fejlődésről:
	- Hány szót kellett lefordíttatni
	- Milyen szinten áll most ezek alapján
		A szint a következő képlettel számolandó:
			alsó_egész(összes_ismert_angol_szavak_száma / összes_angol_szavak_száma * 100)
			ahol az összes_angol_szavak_száma egy közlítőleges érték
		Mutasson névszerű szinteket is:
			A1,A2,B1,B2,C1,C2
			A program a tudott szavak száma alapján döntse ezt el.
			PL: B2 3000-től 5000 szóig
A program mutasson globális statisztikákat is:
	Írja ki a TOP legnehezebb szavak listáját, valamint:
		- hányszor kellett kikérdezni hogy megtanulják
		- hány ember nem tudta
		A nehézség ezen két elem súlyozott összege.
A program biztosítson lehetsőgéget a szavak kikérdezésére.
	A kikérdezés menete:
		Minden szót kérdezzen legalább egyszer.
		Ha elsőre tudta a felhasználó akkor többet már ne kérdezze.
		Ha elsőre nem tudta akkor kérdezze töbször is.
		Ha kikérdezés végetért akkor a program írja a felhasználónak hogy tartson egy rövid Szünetet.
Lehessen be és kijelentkezni (akár google -el).

# nem funkcionális követelmények
Legyen gyors
	Ne kelljen újratölteni a teljes oldal minden menüváltáskor.
	A felhasználó mindig értesüljön róla hogy az oldal elkezdte csinálni a megkezdett tranzakciót
	PL: lehetnek töltési animációk
Legyen elérhető telefonról is

